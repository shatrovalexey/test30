<?php
	/** класс для согласованного выполнения программ пакета
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	namespace Application ;

	/**
	* @subpackage Application\Controller контроллер
	*/
	class Controller extends Base {
		/**
		* Создание объекта
		* @param stdclass $creator ссылка на объект-создатель
		*/
		public function __construct( $creator = null ) {
			// вызов метода родителя
			parent::__construct( $creator ) ;

			/**
			* @var Application\Model\User $User - объект модели пользователя
			*/
			$this->user = new Model\User( $this ) ;
		}

		/**
		* Получение значений HTTP-аргументов.
		* @param array $name имена аргументов HTTP-запроса
		* @return mixed
		* если передано только одно имя аргумента, то возвращается скаляр,
		* если передано много имён, то возвращает массив
		*/
		public function __arg( ) {
			$request = &$this->creator->request ;
			$result = array( ) ;

			foreach ( func_get_args( ) as $name ) {
				if ( ! isset( $request[ $name ] ) ) {
					$result[] = null ;

					continue ;
				}

				$result[] = $request[ $name ] ;
			}

			if ( count( $result ) == 1 ) {
				return $result[ 0 ] ;
			}

			return $result ;
		}

		/**
		* Оформление ответа для вывода в формате JSON
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		* result.data - данные для вывода в теле HTTP-ответа
		* result.passthru - выводить представление не внутри общего представления, а сразу
		* result.key - имя переменной для присвоения массива сообщений
		*/
		protected function __json( &$data ) {
			return array(
				'result' => array(
					'data' => $data
				) ,
				'view' => 'json' ,
				'headers' => array(
					array(
						$this->creator->config->http->header->default_name ,
						$this->creator->config->http->header->javascript
					)
				) ,
				'key' => 'message' ,
				'passthru' => true
			) ;
		}

		/**
		* Идентификаторы пользователя
		* @return array
		* string( 32 ) $session_id - идентификатор сессии
		* int $user_id  - идентификатор пользователя
		*/
		protected function __ids( ) {
			$session_id = $this->__arg( 'session_id' ) ;
			$user = $this->user->info( $session_id ) ;

			return array(
				$session_id ,
				$user[ 'id' ]
			) ;
		}

		/**
		* Главная страница
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function indexAction( ) {
			return array(
				'view' => 'index' ,
				'result' => array( )
			) ;
		}

		/**
		* Создание пользователя
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function registerAction( ) {
			$data = array(
				'fio' => $this->__arg( 'fio' ) ,
				'login' => $this->__arg( 'login' ) ,
				'passwd' => $this->__arg( 'passwd' ) ,
				'photo' => $this->__arg( 'photo' )
			) ;
			$result = array(
				'result' => array( )
			) ;

			try {
				$this->user->create( $data ) ;

				$result[ 'result' ] = array(
					'title' => $this->creator->dict( 'registration_success' )
				) ;
			} catch( \Exception $exception ) {
				$result[ 'result' ][ 'title' ] = $this->creator->dict( 'registration_failure' ) ;
				$result[ 'error' ] = $exception->getMessage( ) ;
			}

			return $this->__json( $result ) ;
		}
	}