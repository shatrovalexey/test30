var $form = jQuery( ".main-form" ).on( "submit" , function( $evt ) {
	var $self = jQuery( this ) ;

	jQuery.ajax( {
		"url" : $self.attr( "action" ) ,
		"type" : $self.attr( "method" ) ,
		"data" : $self.serialize( ) ,
		"dataType" : "json" ,
		"context" : $self ,
		"success" : function( $data ) {
			if ( ! ( "error" in $data.data ) ) {
				$form.find( "label" ).hide( ) ;
			}
			$showMessage( $data.data.result.title , $data.data.error ) ;
		} ,
		"failure" : function( ) {
			$showMessage( $form.find( ".msg span" ).text( ) ) ;
		}
	} ) ;

	return false ;
} ) ;

var $showMessage = function( $title , $message ) {
	$form.find( ".msg-title" ).text( $title ) ;
	$form.find( ".msg-message" ).text( $message ) ;
} ;

jQuery( ".main-form-lang select" ).on( "change" , function( ) {
	var $self = jQuery( this ) ;

	window.location.href = "?lang=" + $self.val( ) ;
} ) ;

$form.find( "input[name=_photo]" ).on( "change" , function( ) {
	if ( ! this.files.length ) {
		$showMessage( "" , $photo.attr( "title" ) ) ;

		return false ;
	}

	$form.prop( "disabled" , true ) ;

	var $file_reader  = new FileReader( ) ;
	$file_reader.onloadend = function( ) {
		$form.find( "input[name=photo]" ).val( $file_reader.result ) ;
		$form.prop( "disabled" , null ) ;
	} ;
	$file_reader.readAsDataURL( this.files[ 0 ] ) ;
} ) ;