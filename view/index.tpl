<form class="main-form" action="register" method="post" enctype="multipart/form-data">
	<input type="hidden" name="photo" required>
	<div class="main-form-lang">
		<select name="lang">
		{foreach from=$config->msg->lang item=label key=lang}
			<option value="{$lang|htmlspecialchars}"{if $app->lang_current==$lang} selected{/if}>{$label|htmlspecialchars}</option>
		{/foreach}
		</select>
	</div>

	<h1>{$app->dict( "registration" )|htmlspecialchars}</h1>
	<label>
		<span>{$app->dict( "fio" )|htmlspecialchars}</span>
		<input name="fio" required pattern="{$config->field->fio|htmlspecialchars}" title="{$app->msg( "err" , "fio" )|htmlspecialchars}">
		<div class="both"></div>
	</label>
	<label>
		<span>{$app->dict( "photo" )|htmlspecialchars}</span>
		<input name="_photo" required title="{$app->msg( "err" , "photo" )|htmlspecialchars}" type="file" autocomplete="off">
		<div class="both"></div>
	</label>
	<label>
		<span>{$app->dict( "login" )|htmlspecialchars}</span>
		<input name="login" required pattern="{$config->field->login|htmlspecialchars}" title="{$app->msg( "err" , "login" )|htmlspecialchars}">
		<div class="both"></div>
	</label>
	<label>
		<span>{$app->dict( "passwd" )|htmlspecialchars}</span>
		<input name="passwd" required pattern="{$config->field->passwd|htmlspecialchars}" type="password" title="{$app->msg( "err" , "passwd" )|htmlspecialchars}">
		<div class="both"></div>
	</label>
	<label class="main-form-submit">
		<input type="submit" value="&rarr;">
		<span>{$app->dict( "register" )|htmlspecialchars}</span>
		<div class="both"></div>
	</label>

	<div class="msg">
		<h2 class="msg-title"></h2>
		<div class="msg-message"></div>
		<span>{$app->msg( "err" , "internal" )|htmlspecialchars}</span>
	</div>
</form>