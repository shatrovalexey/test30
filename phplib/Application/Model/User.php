<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели пользователя
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class User extends \Application\Model {
		/**
		* создать пользователя
		*/
		public function create( $data ) {
			$this->invalidate( $data ) ;
			$this->validLogin( $data[ 'login' ] ) ;

			$sth = $this->dbh->prepare( '
INSERT INTO
	`' . $this->entityName( ) . '`
SET
	`photo` := :photo ,
	`fio` := :fio ,
	`login` := :login ,
	`passwd` := md5( :passwd ) ;
			' ) ;
			$sth->execute( $data ) ;
			$sth->closeCursor( ) ;
		}

		/**
		* проверка существования логина
		* @return boolean - "существует" или "нет"
		*/
		protected function validLogin( $login ) {
			$sth = $this->dbh->prepare( '
SELECT SQL_SMALL_RESULT SQL_CACHE
	true AS `exists`
FROM
	`' . $this->entityName( ) . '` AS `t1`
WHERE
	( `t1`.`login` = :login )
LIMIT 1 ;
			' ) ;
			$sth->execute( array(
				'login' => $login
			) ) ;
			foreach ( $sth as $result ) {
				throw new \Exception( $this->app->msg( 'err' , 'user_exists' ) ) ;
			}
			$sth->closeCursor( ) ;

			return false ;
		}

		/**
		* проверка допустимости логина, пароля или ФИО
		*/
		protected function invalidate( &$data ) {
			foreach ( $data as $key => &$value ) {
				if ( $key == 'photo' ) {
					$this->upload_photo( $value ) ;

					continue ;
				}

				$value = trim( $value ) ;

				if ( ! preg_match( '{' . $this->app->config->field->$key . '}s' , $value ) ) {
					throw new \Exception( $this->app->msg( 'err' , $key ) ) ;
				}
			}
		}

		/**
		* загрузка фотографии
		*/
		protected function upload_photo( &$file ) {
			$file = mb_substr( $file ,
				mb_strpos( $file , $this->app->config->field->photo_cut ) +
				mb_strlen( $this->app->config->field->photo_cut )
			) ;
			$file = base64_decode( $file ) ;
			$file_size = mb_strlen( $file ) ;
			if ( empty( $file ) || empty( $file_size ) || ( $file_size > $this->app->config->field->photo ) ) {
				throw new \Exception( $this->app->msg( 'err' , 'photo' ) ) ;
			}

			$file_path = sprintf( $this->app->config->field->photo_path , md5( uniqid( ) ) ) ;

			if ( ! file_put_contents( $file_path , $file ) ) {
				throw new \Exception( $this->app->msg( 'err' , 'internal' ) ) ;
			}

			$file = $file_path ;
		}
	}