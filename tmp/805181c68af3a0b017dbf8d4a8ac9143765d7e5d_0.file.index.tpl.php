<?php
/* Smarty version 3.1.31, created on 2018-01-16 13:16:17
  from "Z:\home\test30\view\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5dc2e1229a24_24071824',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '805181c68af3a0b017dbf8d4a8ac9143765d7e5d' => 
    array (
      0 => 'Z:\\home\\test30\\view\\index.tpl',
      1 => 1516094170,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5dc2e1229a24_24071824 (Smarty_Internal_Template $_smarty_tpl) {
?>
<form class="main-form" action="register" method="post" enctype="multipart/form-data">
	<input type="hidden" name="photo" required>
	<div class="main-form-lang">
		<select name="lang">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['config']->value->msg->lang, 'label', false, 'lang');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value => $_smarty_tpl->tpl_vars['label']->value) {
?>
			<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang']->value);?>
"<?php if ($_smarty_tpl->tpl_vars['app']->value->lang_current == $_smarty_tpl->tpl_vars['lang']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['label']->value);?>
</option>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

		</select>
	</div>

	<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("registration"));?>
</h1>
	<label>
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("fio"));?>
</span>
		<input name="fio" required pattern="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value->field->fio);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->msg("err","fio"));?>
">
		<div class="both"></div>
	</label>
	<label>
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("photo"));?>
</span>
		<input name="_photo" required title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->msg("err","photo"));?>
" type="file" autocomplete="off">
		<div class="both"></div>
	</label>
	<label>
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("login"));?>
</span>
		<input name="login" required pattern="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value->field->login);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->msg("err","login"));?>
">
		<div class="both"></div>
	</label>
	<label>
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("passwd"));?>
</span>
		<input name="passwd" required pattern="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value->field->passwd);?>
" type="password" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->msg("err","passwd"));?>
">
		<div class="both"></div>
	</label>
	<label class="main-form-submit">
		<input type="submit" value="&rarr;">
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->dict("register"));?>
</span>
		<div class="both"></div>
	</label>

	<div class="msg">
		<h2 class="msg-title"></h2>
		<div class="msg-message"></div>
		<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['app']->value->msg("err","internal"));?>
</span>
	</div>
</form><?php }
}
