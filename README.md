### Тестовое задание ###
* описание в файле "Задание.docx".

### Настройка и установка ###
* `git clone 'https://github.com/shatrovalexey/Test30'`;
* `composer install`;
* создать БД `test30` и пользователя 'test30' с паролем 'test30' для неё;
* загрузить в БД `test30` структуру и данные из файла "database.sql";

### Список файлов ###
* phplib/ - классы проекта
* * config.json - настройки проекта
* * init.php - инициализация окружения для пакета Application
* * Application/ - пакет Application
* * * Base.php - базовый класс для всех прочих
* * * Application.php - обработчик запросов
* * * Controller.php - контроллер
* * * Model.php - общий класс для моделей
* * * Model/ - модели
* * * * User.php - модель пользователя
* * * Router.php - маршрутизатор
* * * View.php - представление
* view/ - представления
* * default.tpl - общее представление ("рамка" вокруг контента)
* * index.tpl - представление главной страницы
* * json.tpl - представление для JSON
* www/ - файлы для входа обработки HTTP-запросов
* * .htaccess - дополнительные настройки Apache2
* * index.php - запуск обработки HTTP-запросов
* * css/ - CSS
* * js/ - JavaScript

### Автор ###
Шатров Алексей Сергеевич <mail[at]ashatrov.ru>